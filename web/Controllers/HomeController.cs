﻿using System;
using Microsoft.AspNetCore.Mvc;

// Home page controller, just use to help check API health
namespace survey.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
