using System;
using Microsoft.AspNetCore.Mvc;
using survey.Models;
using System.Collections.Generic;
using survey.Data;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Web;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Cors.Infrastructure;
using System.IO;

// Survey API, calls DB with entity framework
namespace survey.Controllers
{
    //Controller for Survey API for use by React JavaScript front end
    [Route("api/[controller]")]
    public class SurveyController : Controller
    {
        // database context
        private readonly SurveyContext _context;

        // initialize controller with db context
        public SurveyController(SurveyContext context){
            _context = context;
        }

        // Get list of Surveys
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var surveys = await _context.Surveys.Include(s => s.Questions).ToListAsync();
            foreach(Survey survey in surveys){
                int i = survey.Questions == null? 0 : survey.Questions.Count;
            }
            return Ok(surveys);
        }

        // Create new survey
        [EnableCors("MyPolicy")]
        [HttpPost]
        public IActionResult Create([FromBody] Survey survey)
        {
            var body = "";
            using (var reader = new StreamReader(Request.Body))
            {
                body = reader.ReadToEnd();
        
                // Do something
            }
            if(ModelState.IsValid && survey != null && survey.Desc != null && survey.Desc.Length > 0){
                try{
                    // Save Survey then Questions
                    _context.Add(survey);
                    _context.SaveChanges();
                    foreach(Question question in survey.Questions){
                        question.SurveyID = survey.ID;
                        _context.Add(question);
                    }
                    _context.SaveChanges();
                    return Ok("Success");
                }catch(Exception e){
                    // Log
                    Console.WriteLine(e);
                    return Ok("Failed to save.");    
                }
            } else{
                return Ok("Model state not valid");
            }            
        }
    }
}
