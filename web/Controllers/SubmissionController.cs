using System;
using Microsoft.AspNetCore.Mvc;
using survey.Models;
using System.Collections.Generic;
using survey.Data;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Web;
using Newtonsoft.Json.Linq;

// Submissions API, calls DB with entity framework
namespace survey.Controllers
{
    // Controller for Survey API for use by React JavaScript front end
    [Route("api/[controller]")]
    public class SubmissionController : Controller
    {
        // database context
        private readonly SurveyContext _context;

        // initialize controller with db context
        public SubmissionController(SurveyContext context){
            _context = context;
        }

        // Get list of Submission
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            return Ok(await _context.Submissions.Include(s => s.Answers).ToListAsync());
        }

        // Create new survey
        [HttpPost]
        public IActionResult Create([FromBody] Submission submission)
        {
            if(ModelState.IsValid){
                try{
                    // Save Survey then Questions
                    _context.Add(submission);
                    _context.SaveChanges();
                    foreach(Answer answer in submission.Answers){
                        answer.SubmissionID = submission.ID;
                        _context.Add(answer);
                    }
                    _context.SaveChanges();
                    return Ok("Success");
                }catch(Exception e){
                    // Log
                    Console.WriteLine(e);
                    return Ok("Failed to save.");    
                }
            } else{
                return Ok("Model state not valid");
            }            
        }
    }
}
