using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace survey.Models
{
    public class Submission
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public DateTime FilledOut { get; set; }
        public ICollection<Answer> Answers { get; set; }
    }
}
