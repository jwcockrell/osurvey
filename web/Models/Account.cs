using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace survey.Models
{
    public class Account
    {
        public int ID { get; set; }
        public string Name { get; set; }

        ICollection<Submission> Submissions { get; set; }
    }
}
