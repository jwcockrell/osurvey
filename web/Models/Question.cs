using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace survey.Models
{
    public class Question
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int SurveyID { get; set; }

        public Survey Survey { get; set; }
    }
}
