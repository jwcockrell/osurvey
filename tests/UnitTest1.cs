using System;
using Xunit;
using survey.Controllers;

namespace tests
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
                Assert.True(1 == 1, "Always pass");
        }

        [Fact]
        public void TestController()
        {
            var hc = new HomeController();
            Assert.True(hc.Index() != null, "Can create home controller");
        }
    }
}
